/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.control;

import java.util.List;
import mrysi.appweb.adquisiciones.entity.Licitacion;
import mrysi.appweb.adquisiciones.entity.Pregunta;
import mrysi.appweb.adquisiciones.oad.OadPregunta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gustavo
 */
@RestController
@RequestMapping("/preguntaRest")
public class PreguntaController {
    
    @Autowired
    OadPregunta oadPregunta;
    
    @GetMapping("/{idLicitacion}")
    public List<Pregunta> getPreguntas(@RequestBody Licitacion l){
        return oadPregunta.findBylicitacionPregunta(l);
    }
    
    @PostMapping("")
    public Pregunta insertarPregunta(@RequestBody Pregunta p){
        oadPregunta.save(p);
        return p;
    }
    
    @PutMapping("")
    public Pregunta responderPregunta(@RequestBody Pregunta p){
        oadPregunta.save(p);
        return p;
    }
    
}
