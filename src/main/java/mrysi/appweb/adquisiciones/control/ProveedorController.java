/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.control;

import java.util.List;
import mrysi.appweb.adquisiciones.entity.Proveedor;
import mrysi.appweb.adquisiciones.oad.OadProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gustavo
 */
@RestController
@RequestMapping("/proveedor")
public class ProveedorController {
    
    @Autowired
    OadProveedor oadProveedor;
    
    @GetMapping("")
    public List<Proveedor> getTodos(){
        return oadProveedor.findAll();
    }
    
    @GetMapping("/{rfc}")
    public Proveedor getProveedor(@PathVariable("rfc") String rfc){
        return oadProveedor.findByrfc(rfc);
    }
    
}
