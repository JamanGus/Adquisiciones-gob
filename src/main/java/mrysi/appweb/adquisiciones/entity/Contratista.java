/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gustavo
 */

@Entity
@Table(name= "CONTRATISTA")
public class Contratista implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONTRATISTA")
    private int idContratista;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DIRECCION")
    private String direccion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TELEFONO")
    private String telefono;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "RFCPROVEEDOR")
    private List<Licitacion> ltLicitaciones;

    public Contratista() {
    }

    public Contratista(int idContratista, String nombre, String direccion, String telefono, List<Licitacion> ltLicitaciones) {
        this.idContratista = idContratista;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ltLicitaciones = ltLicitaciones;
    }

    public int getIdContratista() {
        return idContratista;
    }

    public void setIdContratista(int idContratista) {
        this.idContratista = idContratista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String Nombre) {
        this.nombre = Nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String Direccion) {
        this.direccion = Direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String Telefono) {
        this.telefono = Telefono;
    }

    public List<Licitacion> getLtLicitaciones() {
        return ltLicitaciones;
    }

    public void setLtLicitaciones(List<Licitacion> ltLicitaciones) {
        this.ltLicitaciones = ltLicitaciones;
    }
  
}
