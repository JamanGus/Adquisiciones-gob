/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gustavo
 */
@Entity
@Table(name = "LICITACIONES")
public class Licitacion implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDLICITACION")
    private int idLicitacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Column(name = "FECHAINICIO")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    
    @Column(name = "FECHACIERRE")
    @Temporal(TemporalType.DATE)
    private Date fechaCierre;
    
    @JoinColumn(name="IDCONTRATISTA", referencedColumnName = "IDCONTRATISTA")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Contratista contratista;
        
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLicitacion")
    private List<Pregunta> ltPreguntas;
    //private List<PostulacionLicitacion> ltPostulaciones;
    //private List<BasesCondicion> ltBasesCondiciones;

    public Licitacion() {
    }

    public Licitacion(int idLicitacion, String nombre, Date fechaInicio, Date fechaCierre, Contratista contratista, List<Pregunta> ltPreguntas, List<PostulacionLicitacion> ltPostulaciones, List<BasesCondicion> ltBasesCondiciones) {
        this.idLicitacion = idLicitacion;
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaCierre = fechaCierre;
        this.contratista = contratista;
        this.ltPreguntas = ltPreguntas;
//        this.ltPostulaciones = ltPostulaciones;
//        this.ltBasesCondiciones = ltBasesCondiciones;
    }

//    public List<BasesCondicion> getLtBasesCondiciones() {
//        return ltBasesCondiciones;
//    }
//
//    public void setLtBasesCondiciones(List<BasesCondicion> ltBasesCondiciones) {
//        this.ltBasesCondiciones = ltBasesCondiciones;
//    }
//
//    public List<PostulacionLicitacion> getLtPostulaciones() {
//        return ltPostulaciones;
//    }
//
//    public void setLtPostulaciones(List<PostulacionLicitacion> ltPostulaciones) {
//        this.ltPostulaciones = ltPostulaciones;
//    }
    
    public List<Pregunta> getLtPreguntas() {
        return ltPreguntas;
    }

    public void setLtPreguntas(List<Pregunta> ltPreguntas) {
        this.ltPreguntas = ltPreguntas;
    }

    public int getIdLicitacion() {
        return idLicitacion;
    }

    public void setIdLicitacion(int idLicitacion) {
        this.idLicitacion = idLicitacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public Contratista getContratista() {
        return contratista;
    }

    public void setContratista(Contratista contratista) {
        this.contratista = contratista;
    }

    
}
