/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gustavo
 */
public class Pregunta {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDLICITACION")
    private int idPregunta;
    
     @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "PREGUNTA")
    private String pregunta;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "RESPUESTA")
    private String respuesta;
    
    @Column(name = "FECHAPREGUNTA")
    @Temporal(TemporalType.DATE)
    private Date fechaPregunta;
    
    @Column(name = "FECHAREPUESTA")
    @Temporal(TemporalType.DATE)
    private Date fechaRespuesta;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "IDLICITACION")
    private Licitacion licitacionPregunta;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "RFCPROVEEDOR")
    private Proveedor proveedorPregunta;

    public Pregunta(int idPregunta, String pregunta, String respuesta, Date fechaPregunta, Date fechaRespuesta, Licitacion licitacionPregunta, Proveedor proveedorPregunta) {
        this.idPregunta = idPregunta;
        this.pregunta = pregunta;
        this.respuesta = respuesta;
        this.fechaPregunta = fechaPregunta;
        this.fechaRespuesta = fechaRespuesta;
        this.licitacionPregunta = licitacionPregunta;
        this.proveedorPregunta = proveedorPregunta;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Date getFechaPregunta() {
        return fechaPregunta;
    }

    public void setFechaPregunta(Date fechaPregunta) {
        this.fechaPregunta = fechaPregunta;
    }

    public Date getFechaRespuesta() {
        return fechaRespuesta;
    }

    public void setFechaRespuesta(Date fechaRespuesta) {
        this.fechaRespuesta = fechaRespuesta;
    }

    public Licitacion getLicitacionPregunta() {
        return licitacionPregunta;
    }

    public void setLicitacionPregunta(Licitacion licitacionPregunta) {
        this.licitacionPregunta = licitacionPregunta;
    }

    public Proveedor getProveedorPregunta() {
        return proveedorPregunta;
    }

    public void setProveedorPregunta(Proveedor proveedorPregunta) {
        this.proveedorPregunta = proveedorPregunta;
    }
    
    
}
