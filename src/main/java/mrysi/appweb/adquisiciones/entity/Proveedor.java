/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gustavo
 */
@Entity
@Table(name="PROVEEDORES")
public class Proveedor implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "RFC")
    private String rfc;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRERAZONSOCIAL")
    private String NombreRazonSocial;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TELEFONO")
    private String telefono;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBREREPRESENTANTE")
    private String nombreRepresentante;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TIPORAZONSOCIAL")
    private char razonSocial;
    
    @Column(name = "MARCA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "placasAuto")
    private List<Pregunta> ltPreguntas;
    //private List<PostulacionLicitacion> ltPostulaciones;
    //private List<InvitacionConsorcio> ltInvitaciones;

    public Proveedor() {
    }

    public Proveedor(String rfc, String NombreRazonSocial, String telefono, String nombreRepresentante, char razonSocial, Date fechaRegistro, List<Pregunta> ltPreguntas) {
        this.rfc = rfc;
        this.NombreRazonSocial = NombreRazonSocial;
        this.telefono = telefono;
        this.nombreRepresentante = nombreRepresentante;
        this.razonSocial = razonSocial;
        this.fechaRegistro = fechaRegistro;
        this.ltPreguntas = ltPreguntas;
        //this.ltPostulaciones = ltPostulaciones;
    }
    
    public List<Pregunta> getLtPostulaciones() {
        return ltPreguntas;
    }

    public void setLtPostulaciones(List<Pregunta> ltPreguntas) {
        this.ltPreguntas = ltPreguntas;
    }

    
    
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreRazonSocial() {
        return NombreRazonSocial;
    }

    public void setNombreRazonSocial(String NombreRazonSocial) {
        this.NombreRazonSocial = NombreRazonSocial;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreRepresentante() {
        return nombreRepresentante;
    }

    public void setNombreRepresentante(String nombreRepresentante) {
        this.nombreRepresentante = nombreRepresentante;
    }

    public char getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(char razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    
    
}
