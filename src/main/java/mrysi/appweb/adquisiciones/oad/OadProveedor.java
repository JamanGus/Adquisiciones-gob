/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.oad;

import java.io.Serializable;
import mrysi.appweb.adquisiciones.entity.Proveedor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Gustavo
 */
public interface OadProveedor extends JpaRepository<Proveedor, String> {
    
    Proveedor findByrfc(String rfc);
    
}
