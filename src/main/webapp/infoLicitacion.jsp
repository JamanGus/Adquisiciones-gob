<%-- 
    Document   : infoLicitacion
    Created on : 22-ago-2018, 1:16:18
    Author     : Gustavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Información de Licitación</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        <div class="container">
            <h1 class="display-4">Información de Licitación</h1>
            
            <div class="card">
                <div class="card-header bg-info text-white">Información General</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Nombre Licitación:</p></div>
                    </div>
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Fecha Inicio:</p></div>
                    </div>
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Fecha Fin:</p></div>
                    </div>
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Descripción:</p></div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="card">
                <div class="card-header bg-info text-white">Datos del Contratista General</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Nombre Contratista</p></div>
                    </div>
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Dirección:</p></div>
                    </div>
                    <div class="row">
                        <div class="col-md"><p class="font-weight-bold">Teléfono:</p></div>
                    </div>
                </div>
            </div>
            <a href="preguntas.jsp">Realizar Pregunta</a>
        </div>
    </body>
</html>
