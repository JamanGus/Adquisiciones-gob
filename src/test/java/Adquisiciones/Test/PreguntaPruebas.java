package Adquisiciones.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Date;
import mrysi.appweb.adquisiciones.control.LicitacionController;
import mrysi.appweb.adquisiciones.control.PreguntaController;
import mrysi.appweb.adquisiciones.control.ProveedorController;
import mrysi.appweb.adquisiciones.entity.Licitacion;
import mrysi.appweb.adquisiciones.entity.Pregunta;
import mrysi.appweb.adquisiciones.entity.Proveedor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gustavo
 */
public class PreguntaPruebas {
    
    public PreguntaPruebas() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testPreguntar(){
        System.out.println("insertarPregunta");
        java.util.Date fecha = new java.util.Date();
        
        Licitacion licitacion;
        LicitacionController controlLici = new LicitacionController();
        licitacion = controlLici.consultarLicitacion(1);
        
        Proveedor prov;
        ProveedorController controlProv = new ProveedorController();
        prov = controlProv.getProveedor("CPS9011168M0");
        
        Pregunta pregunta = new Pregunta(0, "Esto funcionará?", "", fecha, null, licitacion, prov);
        PreguntaController controlPreg = new PreguntaController();
        
        Pregunta expResult = pregunta;
        Pregunta result = controlPreg.insertarPregunta(pregunta);
        
        assertEquals(expResult, result);
        
        if(!expResult.equals(result)){
            fail("test no Aprobado");
        }
    }
    
    @Test void testContestar(){
        System.out.println("insertarPregunta");
        java.util.Date fecha = new java.util.Date();
        
        Licitacion licitacion;
        LicitacionController controlLici = new LicitacionController();
        licitacion = controlLici.consultarLicitacion(1);
        
        Proveedor prov;
        ProveedorController controlProv = new ProveedorController();
        prov = controlProv.getProveedor("CPS9011168M0");
        
        Pregunta contestar = new Pregunta(0, "NO, no funcionará", "", fecha, fecha, licitacion, prov);
        PreguntaController controlPreg = new PreguntaController();
        
        Pregunta expResult = contestar;
        Pregunta result = controlPreg.responderPregunta(contestar);
        
        assertEquals(expResult, result);
        
        if(!expResult.equals(result)){
            fail("test no Aprobado");
        }
    }
}
